# Mars Ephemeris Generator

Tools to generate a year's worth of Mars ephemeris for the tools used on the
2001 Mars Odyssey GRS Instrument Suite ground data systems.

## Updates:
* 2022-12-13: Helper functions get their own package. NAIF MICE and all
              required kernels are downloaded automatically. For 
              numbererd kernels, the latest is selected automatically.