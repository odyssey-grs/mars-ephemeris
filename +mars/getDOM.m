function [count] = getDOM(dn, season)
%GETDOM DOM is day-of-mars-month, i.e. the day in this 15-degree Mars season/month.
%   Figure out the day of the month by counting back til the start of the month.

count = -1;
curr_season = season;
while curr_season == season
    dn = dn - 1; % Subtract a day.
    ut = datestr(dn, 'yyyy-mm-ddTHH:MM:SS');
    et = cspice_str2et(ut);
    ls = rad2deg(cspice_lspcn('MARS', et, 'NONE'));
    curr_season = floor(ls / 15);
    count = count + 1;
end

end

