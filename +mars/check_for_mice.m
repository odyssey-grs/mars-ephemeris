function [] = check_for_mice()
%CHECK_FOR_MICE Check for NAIF MICE and download it if needed.
%   Checks for the NAIF MICE toolkit. If it isn't found, downloads and
%   unpacks it.

if ~isfolder('mice')
    fprintf('Downloading MICE...');
    link = 'https://naif.jpl.nasa.gov/pub/naif/toolkit//MATLAB/PC_Linux_GCC_MATLAB9.x_64bit/packages/mice.tar.Z';
    file = 'mice.tar.Z';
    websave(file, link);
    system('tar xf mice.tar.Z && rm mice.tar.Z');
    fprintf('done.\n');
end

end

