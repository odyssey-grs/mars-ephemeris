function [latest] = latest_sclkscet()
%LATEST_SCLKSCET Get latest sclk scet kernel.
%   Scrape the web NAIF website for the name of the latest sclk scet.

fprintf('Getting name of latest sclk scet...');
[~, name] = system('wget -q -O- https://naif.jpl.nasa.gov/pub/naif/M01/kernels/sclk/ | grep ORB1_SCLKSCET | sort | tail -1 | sed -e ''s/.*"ORB1/ORB1/'' -e ''s/".*//''');
name = strtrim(name);
url = strcat('https://naif.jpl.nasa.gov/pub/naif/M01/kernels/sclk/', name);
latest = {strcat('./kernels/', name), url};
fprintf('done.\n');

end

