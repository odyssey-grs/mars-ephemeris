function download(x, y)
%DOWNLOAD Very simple wrapper for downloading a file.
%   Don't download it if it already exists.

if(~exist(x, 'file'))
        websave(x, y);
end

end

