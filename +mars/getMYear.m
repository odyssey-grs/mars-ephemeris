function [mars_year] = getMYear(dn)
%GETMYEAR Get the Mars year of the date.
%   Assumes at least year 10.

mars_year = 10;
start_11 = datenum('2021-Feb-08', 'yyyy-mm-dd');
start_12 = datenum('2022-Dec-27', 'yyyy-mm-dd');
if dn >= start_11 && dn < start_12
    mars_year = 11;
end
if dn >= start_12
    mars_year = 12;
end

end

