%%%%%%%%%%%%%%%%%%%%
% mars_ephemeris.m
% 
% A script for generating ephemeris for the use by the ground data systems
% of the 2001 Mars Odyssey GRS Instrument Suite.
%
% Copyright (c) 2020-2022 The Arizona Board of Regents on behalf of the 
% University of Arizona. All rights reserved.
%
% This program is free software: you can redistribute it and/or modify it 
% under the terms of the GNU General Public License as published by the 
% Free Software Foundation, either version 3 of the License, or (at your 
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but WITHOUT 
% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
% FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for %
% more details.
% 
% You should have received a copy of the GNU General Public License along 
% with this program. If not, see <https://www.gnu.org/licenses/>.

%% Setup.
mars.check_for_mice();
addpath('mice/lib/');
addpath('mice/src/mice');
cspice_version = cspice_tkvrsn('TOOLKIT');
year = 2025;
latest_sclkscet = mars.latest_sclkscet();

%% Load kernels for computing ephemeris.
kernels = {...
    {'./kernels/latest_leapseconds.tls', 'https://naif.jpl.nasa.gov/pub/naif/generic_kernels/lsk/latest_leapseconds.tls'}, ...
    {'./kernels/de432s.bsp', 'https://naif.jpl.nasa.gov/pub/naif/generic_kernels/spk/planets/de432s.bsp'}, ...
    {'./kernels/pck00010.tpc', 'https://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/pck00010.tpc'}, ...
    {'./kernels/mar063l.bsp', 'http://naif.jpl.nasa.gov/pub/naif/M01/kernels/spk/mar063l.bsp'}, ...
    latest_sclkscet ...
    };

fprintf('Downloading kernels...');
if ~isfolder('kernels')
    mkdir('kernels')
end
cellfun(@(x) mars.download(x{1}, x{2}), kernels);
fprintf('done.\n');

%% Load kernels.
fprintf('Loading kernels...');
cspice_kclear();
fprintf('%s...\n', cspice_tkvrsn('TOOLKIT'));
for i=1:length(kernels)
    fprintf('\t%s\n', kernels{i}{1});
    cspice_furnsh(kernels{i}{1})
end
fprintf('done.\n');
    
%% Create the set of days and times, compute ephemeris.
fprintf('Computing ephemeris...');
start_dn = datenum(sprintf('%4d-01-01T00:00:00', year), 'yyyy-mm-ddTHH:MM:SS');
end_dn = datenum(sprintf('%4d-12-30T00:00:00', year), 'yyyy-mm-ddTHH:MM:SS');
DN = zeros(end_dn-start_dn,1);
UT = cell(end_dn-start_dn, 1);
ET = zeros(end_dn-start_dn,1);
LS = zeros(end_dn-start_dn,1);
SC = zeros(end_dn-start_dn,1);
idx = 1;
fid1 = fopen('mars_ephem.csv', 'w');
fid2 = fopen('mars_ephem.sql', 'w');
for dn=start_dn:1:end_dn-1
    DN(idx) = dn;
    UT{idx} = datestr(dn, 'yyyy-mm-ddTHH:MM:SS');
    ET(idx) = cspice_str2et(UT{idx});
    LS(idx) = rad2deg(cspice_lspcn('MARS', ET(idx), 'NONE'));
    SC(idx) = cspice_sce2c(-53,ET(idx));

    season = floor(LS(idx) / 15);
    seasonFraction = LS(idx) / 15;
    dom = mars.getDOM(dn, season);
    myear = mars.getMYear(dn);
    mweek = floor(LS(idx) / 3.75);
    sclk = cspice_sce2s(-53, ET(idx));

    % Print out the day's data.
    fprintf(fid1, '%s, %0.1f, %d, %d, %d, %d, %0.5f, %s \n', ...
        datestr(dn, 'yyyy-mmm-dd'), ...
        LS(idx), ...
        season, ...
        dom, ...
        myear, ...
        mweek, ...
        seasonFraction, ...
        sclk(3:end));
    fprintf(fid2, 'INSERT INTO COMMON.SEASONAL_TAB VALUES (TO_TIMESTAMP(''%s'', ''YYYY-Mon-DD''), %0.1f, %d, %d, %d, %d);\n', ...        
        datestr(dn, 'yyyy-mmm-dd'), ...
        LS(idx), ...
        season, ...
        dom, ...
        myear, ...
        mweek);

    % Next day.
    idx = idx + 1;
end
fclose(fid1);
fclose(fid2);
fprintf('done.\n');
